#!/bin/bash

# Adding gruvbox colorscheme to .vim/colors/
git clone https://github.com/morhetz/gruvbox.git
mkdir -p ~/.vim/colors/
mv gruvbox/colors/gruvbox.vim ~/.vim/colors/
git gc --prune=all
rm -r gruvbox/

# Adding dotfiles to system
mv dotfiles/vimrc ~/.vimrc
mv dotfiles/xinitrc ~/.xinitrc
mv dotfiles/tmux.conf ~/.tmux.conf
#rm -r config
